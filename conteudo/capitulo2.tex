\chapter{Testes de Software}\label{cpTestes}
	Os testes de software constituem um processo que apresenta como principal objetivo localizar erros no software. Eles podem ir desde testes de baixo nível, que verificam isoladamente cada funcionalidade do código, a testes de alto nível, que possuem como finalidade validar se o software atende aos requisitos iniciais. O processo de criação de testes é de grande importância no desenvolvimento do software, pois se esses forem bem desenvolvidos, possibilitam a descoberta de erros ainda no processo de desenvolvimento, ou seja, antes da entrega do software ao cliente \cite{Delamaro2007,Pressman2006, Myers2004}.
	
	A descoberta de erros pode ser realizada fazendo uso de inúmeras atividades, que são denominadas de verificação e validação. A verificação corresponde à agregação de várias atividades com a finalidade de assegurar que o software implemente uma respectiva função de forma correta. Já a validação refere-se a um conjunto de atividades realizadas com o propósito de garantir que o software atenda aos requisitos iniciais \cite{Delamaro2007,Pressman2006, Myers2004}.

	A utilização das atividades como a verificação e validação contribuem para um ganho na qualidade de um software. Segundo a IEEE 610.12-1990 \cite{IEEE1990} pode-se definir qualidade de software,  como o grau de um sistema, componente ou processo em satisfazer as especificações dos requisitos e as necessidades ou expectativas do usuário. Já a NBR ISO 9000-2005 \cite{NBRISO9000} define a qualidade de software como o grau no qual um conjunto de características satisfazem os requisitos \cite{Rocha2001}. Embora não seja possível, através de testes, provar que um software esteja totalmente correto, testes criteriosos contribuem para aumentar a qualidade do software.

\newpage

\section{Técnicas de Testes de Software}
	Os testes de software são realizados com a finalidade de localizar erros. Segundo  \cite{Sommerville2003}:
\begin{quote}
    ``Um teste bem sucedido para a detecção de defeitos é aquele que faz com que o sistema opere incorretamente e, como consequência, expõe um defeito existente. Isso enfatiza um fato importante sobre os testes e demonstra a presença, e não a ausência, de defeitos de programa.''
\end{quote}

	A Figura \ref{img_modelo_geral_processo_teste} apresenta um modelo geral para o processo para a realização de teste em um software. Esse modelo inicia com a atividade de ``Projetar casos de teste'', onde definem-se os casos de teste a serem realizados. Já na etapa ``Preparar dados de testes'' são definidos os parâmetros de entradas, de saída previamente conhecidos e, por fim, a descrição do conteúdo a ser testado. Os dados de testes são os elementos de entrada para a fase seguinte que consiste na execução do teste. A etapa ``Executar programa com os dados de teste'' corresponde a execução do programa propriamente dito. Como entrada são utilizados os dados definidos na etapa anterior. Por fim, na etapa ``Comparar resultados com os casos de teste'' o resultado dos testes é comparado com os dados de saída previamente conhecidos. Se esse retorno for igual ao resultado esperado, pode-se assumir que não foram identificados erros neste programa \cite{Sommerville2003}.

\FloatBarrier
\begin{figure}[!ht]
\centering
\includegraphics[width=0.4\hsize]{imagens/modelo_geral_processo_teste.png}
\caption{Modelo geral do processo de teste de um software (Adaptado de \cite{Sommerville2003})}
\label{img_modelo_geral_processo_teste}
\end{figure}
\FloatBarrier

\newpage

	Existem várias técnicas de testes para softwares, sendo que as mais utilizadas e referenciadas são os testes estruturais, funcionais e mais recentemente os automatizados. Nas próximas seções será apresentada uma descrição dessas técnicas.
	
\subsection{Teste Estrutural}
	Os testes estruturais, também conhecidos como testes de caixa branca, são os testes que abrangem a estrutura interna do software. Nesse tipo de teste as partes do código são testadas de forma isolada, com a finalidade de garantir que todo o código seja executado ao menos uma vez com sucesso. Todas as estruturas de controle do código são cobertos por esta técnica, podendo ser testados os retornos de métodos, bem como códigos estruturados. Esses testes são executados quando o código fonte é finalizado e, além disso, os responsáveis pela execução dos testes necessitam ter um conhecimento estrutural da implementação do software \cite{Pressman2006,Sommerville2003}.

	Fazendo uso da técnica de testes estruturais, o responsável pelo processo de teste pode construir casos de teste direcionados às seguintes atividades \cite{Pressman2006}:

\begin{itemize}
    	\item Garantir que todos os caminhos independentes de um módulo tenham sido visitados pelo menos uma vez;
	\item Executar todas as decisões lógicas em seus lados verdadeiro e falso;
	\item Executar todos os ciclos nos seus limites e dentro de seus intervalos operacionais;
	\item Executar as estruturas de dados internas de forma a garantir sua validade.
\end{itemize}

	A construção dos casos de testes estruturais, em sua grande maioria utiliza um grafo de fluxo ou grafo de programa para representar o fluxo de controle lógico do software. No grafo de fluxo, os nós representam os comandos procedimentais e as arestas, os fluxos de controle \cite{Delamaro2007}.
	
	O Trecho de Código \ref{lstExemploEstrutural} exemplifica o uso da técnica estrutural. Esse Trecho de Código corresponde a uma função que permite localizar um caractere em uma \textit{string} e foi desenvolvido na linguagem de programação Python \cite{Python2013}. O Trecho de Código foi implementado usando uma lógica mais simplista apenas para exemplificar um fluxo mais completo. Dessa forma, neste exemplo não foram considerados fatores como estrutura e desempenho de código.
	
	A Figura \ref{img_grafo_fluxo} é a representação do Trecho de Código \ref{lstExemploEstrutural} em forma de um grafo de fluxo. Esse grafo é formado por 7 nós e 8 arestas, sendo que o nó 1 corresponde às linhas 9 e 10, as linhas 11, 12, 13, 14, 15 e 16 são representadas pelos nós 2, 3, 4, 5, 6 e 7, respectivamente. O comando \textit{if} na linha 12 e o comando \textit{break} na linha 14 representam um desvio de execução entre os nós. Se a condição de teste do comando \textit{if} for verdadeira então ocorre um desvio de execução do nó 3 para o nó 4, em caso contrário o desvio de execução ocorre do nó 3 para o nó 7, bem como o comando \textit{break} na linha 14, se executado, causa um desvio de execução do nó 5 para o nó 6 e de maneira semelhante os demais desvios são constituídos.

\FloatBarrier
\begin{lstlisting}[caption={Função em Python para a localização de um caractere em uma \textit{string}},label={lstExemploEstrutural}]
#-*- coding: utf-8 -*-

def get_posicao(palavra, letra):
    """ 
        Encontra uma letra em um palavra.
        Recebe uma palavra<string> e uma letra<string>.
        Retorna a posicao da letra na palavra se encontrar, senao retorna -1.
    """
    cont = 0 
    posicao = -1
    while len(palavra) > cont:
        if palavra[cont] == letra:
            posicao = cont
            break
        cont += 1
    return posicao
\end{lstlisting}
\FloatBarrier

\FloatBarrier
\begin{figure}[!ht]
\centering
\includegraphics[width=0.4\hsize]{imagens/grafo_de_fluxo_estrutural.png}
\caption{Grafo de Fluxo referente ao Trecho de Código \ref{lstExemploEstrutural}}
\label{img_grafo_fluxo}
\end{figure}
\FloatBarrier

	A definição do grafo de fluxo possibilita estabelecer caminhos que definem a transição entre os nós do grafo. Se forem desenvolvidos testes que executem esses caminhos, todos os comandos do software terão sido executados pelo menos uma vez. O problema é definir o número de caminhos a serem executados. Esse problema pode ser solucionado utilizando o cálculo da complexidade ciclomática. A complexidade ciclomática é uma medida que fornece a complexidade lógica de um software. Essa medida utilizada no âmbito de testes define o número limite supremo de caminhos a serem percorridos no grafo. Esse número limite determina a quantidade de testes que devem ser desenvolvidos para garantir que todos os comandos sejam executados pelo menos uma vez. A complexidade ciclomática, $v(G)$, é definida para um grafo de fluxo como \cite{Pressman2006}:

\begin{quote}
	\item $v(G) = E - N + 2$
	\item onde $E$ é o número de arestas e $N$ é o número de nós. 
\end{quote}

	A execução do calculo $v(G)$ para a Figura \ref{img_fluxograma_get_posicao} do grafo de fluxo resulta em 3 caminhos, $v(G) = 8 - 7 + 2 = 3$. De fato, os possíveis caminhos são:

\begin{itemize}
	\item caminho 1: 1,2 e 6
	\item caminho 2: 1,2,3,7,2 e 6
	\item caminho 3: 1,2,3,4,5 e 6
\end{itemize}

Desta forma, uma estratégia de teste de software pode ser definida para cada caminho independente de um programa, sendo que a quantidade de casos de teste é definida pela complexidade ciclomática do programa.

\subsection{Teste Funcional}
	Os testes funcionais são baseados nas especificações iniciais do sistema e possuem como finalidade validar se o sistema implementa as funcionalidades que foram definidas na fase dos requisitos. Os testes funcionais também são conhecidos como testes de caixa preta, uma vez que não abordam o funcionamento interno do código. Nesse tipo de testes são informados apenas os dados de entrada para uma rotina de teste, sendo que o resultado é comparado com resultados previamente conhecidos. Segundo \cite{Pressman2006}, um teste de caixa preta visa verificar as funcionalidades do processo como um todo e não funcionalidades estruturais internas.
	
	A técnica de teste funcional pode ser utilizada nos mais diversos tipos de software. No Trecho de Código \ref{lstExemploFuncional} tem-se uma função que busca a posição de um caractere em uma \textit{string} e, caso o caractere não seja encontrado, a função retorna -1. Pode-se aplicar a técnica funcional nesse software mesmo sem conhecer a estrutura interna do código, sendo que para isso basta conhecer os parâmetros de entrada e os dados de retorno. O fluxograma da Figura \ref{img_fluxograma_get_posicao} representa o funcionamento geral do Trecho de Código \ref{lstExemploFuncional}. Com o conhecimento do funcionamento geral do software o testador de código pode definir os parâmetros de entrada, efetuar a execução do software de forma manual e analisar os retornos. A partir da análise dos valores de retorno é possível validar se o software atende aos requisitos iniciais.

\FloatBarrier
\begin{lstlisting}[caption={Exemplo de software otimizado que localiza um caractere em uma \textit{string}},label={lstExemploFuncional}]
#-*- coding: utf-8 -*-

def get_posicao(palavra, letra):
    """ 
        Encontra uma letra em um palavra.
        Recebe uma palavra<string> e uma letra<string>.
        Retorna a posicao da letra na palavra se encontrar, senao retorna -1.
    """
    posicao = palavra.find(letra)
    if posicao >= 0:
        return posicao
    else:
        return -1

\end{lstlisting}
\FloatBarrier


\FloatBarrier
\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\hsize]{imagens/fluxograma_get_posicao.png}
\caption{Fluxograma referente ao Trecho de Código \ref{lstExemploFuncional}}
\label{img_fluxograma_get_posicao}
\end{figure}
\FloatBarrier

\newpage

\subsection{Testes Automatizados}
	Os testes automatizados são códigos que testam as funcionalidades do sistema de forma automática, podendo ser disparados inúmeras vezes no processo de desenvolvimento do sistema. A execução dos testes automatizados pode ser agendada em servidores específicos ou ainda quando os computadores estão ociosos. A utilização dessa prática agrega ganhos futuros, pois após a criação dos testes os desenvolvedores reduzem o tempo necessário para a realização de testes manuais e, consequentemente, aumentam o tempo disponível para o desenvolvimento de novas tarefas \cite{Fewster1999}.

	A utilização de testes automatizados acarreta em ganho de tempo e qualidade ao software, quando comparada com a realização de testes manuais. Um caso de teste pode ser executado manualmente de forma rápida, mas torna-se mais trabalhoso quando executado manualmente inúmeras vezes. Além disso, a tarefa de comparar resultados de um grande volume de dados é extremamente exaustiva para um humano, aumentando a possibilidade de ser efetuada com erros. 
	
	Nesse contexto, o emprego da técnica de automatização de testes resultará na diminuição do tempo de execução do processo de testes, proporcionando um aumento da produtividade. Além disso, essa técnica pode ser empregada para testes de desempenho e carga de software, as quais são complexas de serem testadas de forma manual em um ambiente de desenvolvimento \cite{Bernardo2008, Fewster1999}.
	
\subsubsection{Ferramentas para automatização de testes}
	
	A automatização de testes é proporcionada por ferramentas ou \textit{frameworks} que possibilitam a execução dos testes de forma automatizada. Essas ferramentas possibilitam que o desenvolvedor crie casos de testes com a finalidade de validar os códigos desenvolvidos. São inúmeras as ferramentas para automatização de testes, sendo que cada linguagem de programação possui uma ou mais ferramentas. Segundo o  \textit{Software QA and Testing Resource Center}\footnote{http://www.softwareqatest.com/qatweb1.html}
existem mais de 500 ferramentas sobre testes e qualidade de software. A seguir são apresentados, apenas em caráter de exemplificação, três \textit{frameworks} que são utilizados para automatização de testes em linguagens de programação distintas:
		
\begin{itemize}
	\item O \textit{JUnit} é um \textit{framework} que possibilita a criação de códigos com a finalidade de automatizar testes para a linguagem de programação Java. Foi criado por Kent Beck e Erich Gamma baseado no \textit{framework} de testes do \textit{Smalltalk}. Mais informações sobre o \textit{JUnit} podem ser encontradas em: \textit{http://www.junit.org}.

	\item O \textit{Unittest} \cite{Unittest2013}, também chamado de \textit{PyUnit}, é um \textit{framework} para testes unitários baseado no \textit{JUnit}, escrito para linguagem de programação \textit{Python}. Esse foi iniciado por Steve Purcell, sendo introduzido na versão do \textit{Python} 2.1, tornando-se biblioteca padrão desde então. Na Seção \ref{subExemploTesteUnidade} é exemplificado o uso desse \textit{framework}, uma vez que esse será o \textit{framework} utilizado no desenvolvimento da solução proposta neste trabalho.

	\item O \textit{PHPUnit} é uma ferramenta para automatizar testes de unidade na linguagem de programação \textit{PHP}. Esse foi criado por Sebastian Bergmann, baseado no \textit{JUnit}. Mais informações sobre esse podem ser encontradas em: \textit{http://phpunit.de}.
\end{itemize}
	
\section{Estratégias de Testes}
	Há inúmeras estratégias que podem ser aplicadas para testes de software, sendo que essas estratégias podem ser aplicadas em diferentes tipos de testes. Por exemplo, essas podem ser utilizadas para validar funcionalidades específicas, como um trecho de código, interfaces, integrações de processos, teste de carga, entre outros. A seguir serão descritas algumas estratégias de testes, dando ênfase na estratégia de teste de unidade, pois essa será a abordagem utilizada para o desenvolvimento desse trabalho.

\subsection{Teste de Unidade}
	O teste de unidade ou teste unitário é usado na localização de erros dentro do limite interno de funções, métodos e pequenos trechos de códigos. Pode-se utilizar o teste de unidade para verificar se os métodos ou funções retornam os resultados corretos, se cada fluxo de controle está funcionando como esperado e, por fim, se a lógica de um trecho de código está coerente com os requisitos iniciais \cite{Teles2009,Pressman2006}.

	O teste de unidade facilita a organização do processo de teste uma vez que prioriza pequenas unidades ou módulos do programa. À medida que erros são detectados, eles podem facilmente ser corrigidos em escala menor, facilitando também a tarefa de depuração. Além disso, os testes de unidade podem ser executados simultaneamente em vários módulos do programa.

	A ideia básica do teste unitário é examinar o comportamento do código sob condições variadas, baseado em dados de entrada e nos resultados esperados. As ``unidades'' testadas geralmente se referem a pequenos módulos de códigos como, por exemplo, funções, procedimentos, métodos ou classes. É importante destacar que o teste de unidade verifica apenas o método ou classe focada pelo programador, independente de recursos externos como, por exemplo, sistema de arquivos, banco de dados, rede, etc.

	A criação dos testes de unidade pode ser efetuada antes ou após o início da fase de desenvolvimento. A diferença entre essas duas direções é que ao se desenvolver os testes de unidade antes da codificação, o desenvolvedor focalizará mais na compreensão do problema e, consequentemente, estará ampliando a análise do problema \cite{Teles2009}. Assim, uma boa compreensão do problema e uma base adequada de testes de unidade, levará o desenvolvedor a programar somente o necessário evitando códigos desnecessários e podendo diminuir, em muitas vezes, a complexidade do código. A abordagem em se desenvolver os testes de unidade no final da codificação também é válida, pois permite a reexecução dos testes, garantindo que novas alterações não provoquem erros.

	As abordagens ágeis para o desenvolvimento de software adotam que os testes de unidade sejam desenvolvidos antes da codificação e sejam automatizados. A automatização dos testes de unidade é proporcionada por ferramentas que facilitam a reexecução dos testes. Essas ferramentas possibilitam a execução dos testes a qualquer momento no processo de codificação, permitindo que o programador execute esses testes inúmeras vezes.

\subsubsection{Exemplo da utilização dos testes de unidade}\label{subExemploTesteUnidade}

    Nessa seção será exemplificado a utilização dos testes de unidade de forma automatizada usando o \textit{framework} Python \textit{Unittest} \citep{Unittest2013}. Nesse exemplo é apresentado o desenvolvimento de um software, cujo objetivo é converter valores da base hexadecimal para a base decimal. O desenvolvimento será conduzido pelo método ágil onde primeiramente tem-se a codificação dos casos de testes.
    
    No Trecho de Código \ref{lstExemploUnitarioTeste1} tem-se o código referente aos casos de teste,  que permitirão a validação da classe \textit{CalculadoraSimples()}. No método \textit{setUp()} é feito o instanciamento da classe \textit{CalculadoraSimples()}. O método \textit{test\_{}conversao()} tem como finalidade validar os retornos do método \textit{converte\_{}hex\_{}para\_{}dec()}, que é responsável pela conversão dos valores passados por parâmetros, de hexadecimal para decimal. A validação do código à ser testado é feita através da execução de métodos da classe \textit{unittest.TestCase()}. Nesse caso é utilizado o método \textit{assertEqual()} que recebe como primeiro parâmetro o método a ser testado e como segundo parâmetro o resultado de retorno esperado da execução do código. A execução do método \textit{assertEqual()} resulta em sucesso se o resultado da execução da função (primeiro parâmetro) é igual ao resultado esperado (segundo parâmetro), caso contrário é apresentado um erro. É utilizado ainda o método \textit{assertFalse()}, também da classe \textit{unittest.TestCase()}, que tem como finalidade verificar um caso onde a execução do código resulta Falso. A passagem dos parâmetros é semelhante aos do método \textit{assertEqual()}.

\newpage

\FloatBarrier
\begin{lstlisting}[caption={Exemplo do código para automatizar os testes de unidade para a classe CalculadoraSimples.},label={lstExemploUnitarioTeste1}]

#-*- coding: utf-8 -*-
import unittest
from exemplo_unitario import *

class TestUnitarios(unittest.TestCase):

    def setUp(self):
        self.calculadora = CalculadoraSimples()

    def test_conversao(self):
        self.assertEqual(self.calculadora.converte_hex_in_dec("0123456789"), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) 
        self.assertEqual(self.calculadora.converte_hex_para_dec("abcdef"), [10, 11, 12, 13, 14, 15])
        self.assertEqual(self.calculadora.converte_hex_para_dec("0123456789abcdef"), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        self.assertFalse(self.calculadora.converte_hex_para_dec("xyz"), []) 

if __name__ == '__main__':
    unittest.main()

\end{lstlisting}
\FloatBarrier

	Na primeira execução do Trecho de Código \ref{lstExemploUnitarioTeste1} é esperado que todos os testes falhem, como pode ser observado na linha 14 do Trecho de Código \ref{lstExemploUnitarioExecucaoTeste1}, que apresenta a saída ``\textit{FAILED (failures=1)}''. Isso ocorre pois ainda não foi desenvolvido o método que é responsável pela conversão dos valores da classe \textit{CalculadoraSimples()}.

\FloatBarrier
\begin{lstlisting}[caption={Exemplo da execução do código de teste utilizando o \textit{framework} Python \textit{unittest}.},label={lstExemploUnitarioExecucaoTeste1}]
luciano@luciano-XPS-L421X:~/Tcc_Luciano/scripts$ python exemplo_unitario_tests.py 
F
======================================================================
FAIL: test_conversao (__main__.TestUnitarios)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "exemplo_unitario_tests.py", line 11, in test_conversao
    self.assertEqual(self.calculadora.converte_hex_para_dec("0123456789"), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
AssertionError: None != [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

----------------------------------------------------------------------
Ran 1 test in 0.002s

FAILED (failures=1)

\end{lstlisting}
\FloatBarrier

	No Trecho de Código \ref{lstExemploUnitario1}, tem-se o código da classe \textit{CalculadoraSimples()} e do método \textit{converte\_{}hex\_{}para\_{}dec()}. Esse método faz a conversão de valores hexadecimal em decimal, recebendo como parâmetro uma \textit{string} e retornando uma lista com as conversões ou uma lista vazia caso ocorra algum erro.
	
	A partir da finalização do Trecho de Código \ref{lstExemploUnitario1}, pode-se reexecutar os testes do Trecho de Código \ref{lstExemploUnitarioTeste1}. No Trecho de Código \ref{lstExemploUnitarioExecucaoTeste2} tem-se o resultado dos testes, onde pode-se observar que todos os testes resultaram em sucesso. A partir de uma análise mais detalhada do código observa-se que um caso de teste foi esquecido, ou seja, faltou testar a execução do método \textit{converte\_{}hex\_{}para\_{}dec()} utilizando-se letras maiúsculas como parâmetros de entrada. Como o desenvolvimento é dirigido a testes tem-se \cite{Teles2009}:

\begin{enumerate}
	\item Criação de novos casos de teste
	\item Execução dos casos de teste, sendo esperado que esses falhem
	\item Efetuar as devidas correções no código
	\item Reexecução dos testes e verificar se os mesmos retornam sucesso
\end{enumerate}	


\FloatBarrier
\begin{lstlisting}[caption={Exemplo de uma simples calculadora que converte um número de Hexadecimal em Decimal.},label={lstExemploUnitario1}]
#-*- coding: utf-8 -*-

class CalculadoraSimples(object):

    def converte_hex_para_dec(self, dados_hex):
        """
            Efetua conversoes de Hexadecimal em Decimal
            Retorna uma lista<list> com as convercoes, senao retorna uma lista vazia[]
        """
        numeros = ['0','1','2','3','4','5','6','7','8','9']
        retorno = []
        for dado in dados_hex:
            if dado in numeros:
                retorno.append(int(dado))
            elif dado == "a":
                retorno.append(10)
            elif dado == "b":
                retorno.append(11)
            elif dado == "c":
                retorno.append(12)
            elif dado == "d":
                retorno.append(13)
            elif dado == "e":
                retorno.append(14)
            elif dado == "f":
                retorno.append(15)
            else:
                return []
        return retorno

\end{lstlisting}
\FloatBarrier

\newpage

\FloatBarrier
\begin{lstlisting}[caption={Exemplo da execução do código de teste utilizando o \textit{framework} Python \textit{unittest}, tendo como saída um caso de sucesso.},label={lstExemploUnitarioExecucaoTeste2}]
luciano@luciano-XPS-L421X:~/Tcc_Luciano/scripts$ python exemplo_unitario_tests.py 
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
\end{lstlisting}
\FloatBarrier

	O Trecho de Código \ref{lstExemploUnitarioTeste2} ilustra o caso de teste para validar se o método \textit{converte\_{}hex\_{}para\_{}dec()} aceita letras maiúsculas. Após a nova execução dos testes, é esperado uma falha, como é mostrado no Trecho de Código \ref{lstExemploUnitarioExecucaoTeste3}.

\FloatBarrier
\begin{lstlisting}[caption={Exemplo do código para automatizar os testes de unidade para a classe CalculadoraSimples, com o caso de teste para validar letras maiúsculas.},label={lstExemploUnitarioTeste2}]

#-*- coding: utf-8 -*-
import unittest
from exemplo_unitario import *

class TestUnitarios(unittest.TestCase):

    def setUp(self):
        self.calculadora = CalculadoraSimples()

    def test_conversao(self):
        self.assertEqual(self.calculadora.converte_hex_para_dec("0123456789"), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.assertEqual(self.calculadora.converte_hex_para_dec("abcdef"), [10, 11, 12, 13, 14, 15])
        self.assertEqual(self.calculadora.converte_hex_para_dec("0123456789abcdef"), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        self.assertFalse(self.calculadora.converte_hex_para_dec("xyz"), [])
        self.assertEqual(self.calculadora.converte_hex_para_dec("ABCDEF"), [10, 11, 12, 13, 14, 15])

if __name__ == '__main__':
    unittest.main()

\end{lstlisting}
\FloatBarrier

\FloatBarrier
\begin{lstlisting}[caption={Exemplo da execução do código de teste utilizando o \textit{framework} Python \textit{unittest}. Inserção de um novo caso de teste. Caso com falha.},label={lstExemploUnitarioExecucaoTeste3}]
luciano@luciano-XPS-L421X:~/Tcc_Luciano/scripts$ python exemplo_unitario_tests.py 
F
======================================================================
FAIL: test_conversao (__main__.TestUnitarios)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "exemplo_unitario_tests.py", line 15, in test_conversao
    self.assertEqual(self.calculadora.converte_hex_para_dec("ABCDEF"), [10, 11, 12, 13, 14, 15])
AssertionError: Lists differ: [] != [10, 11, 12, 13, 14, 15]

Second list contains 6 additional elements.
First extra element 0:
10

- []
+ [10, 11, 12, 13, 14, 15]

----------------------------------------------------------------------
Ran 1 test in 0.001s

FAILED (failures=1)
\end{lstlisting}
\FloatBarrier


	No Trecho de Código \ref{lstExemploUnitario2}, tem-se o método \textit{converte\_{}hex\_{ }para\_{}dec()} alterado (linha 12) de forma que seja aceito letras maiúsculas. Novamente, são executados os testes do Trecho de Código \ref{lstExemploUnitarioTeste2}, sendo que após a execução verifica-se que os testes retornaram sucesso (\ref{lstExemploUnitarioExecucaoTeste4}). Com a execução dos testes bem sucedida, o processo de desenvolvimento do software de exemplo pode ser considerado concluído.

\FloatBarrier
\begin{lstlisting}[caption={Exemplo de uma simples calculadora que converte Hexadecimal em Decimal, aceitando letras maiúsculas e minúsculas como parâmetro.},label={lstExemploUnitario2}]
#-*- coding: utf-8 -*-

class CalculadoraSimples(object):

    def converte_hex_para_dec(self, dados_hex):
        """
            Efetua conversoes de Hexadecimal em Decimal
            Retorna uma lista<list> com as convercoes, senao retorna uma lista vazia[]
        """
        numeros = ['0','1','2','3','4','5','6','7','8','9']
        retorno = []
        dados_hex = dados_hex.lower()
        for dado in dados_hex:
            if dado in numeros:
                retorno.append(int(dado))
            elif dado == "a":
                retorno.append(10)
            elif dado == "b":
                retorno.append(11)
            elif dado == "c":
                retorno.append(12)
            elif dado == "d":
                retorno.append(13)
            elif dado == "e":
                retorno.append(14)
            elif dado == "f":
                retorno.append(15)
            else:
                return []
        return retorno

\end{lstlisting}
\FloatBarrier

\newpage

\FloatBarrier
\begin{lstlisting}[caption={Exemplo da execução do código de teste utilizando o \textit{framework} Python \textit{unittest}. Com a correção para aceitar letras maiúsculas.},label={lstExemploUnitarioExecucaoTeste4}]

luciano@luciano-XPS-L421X:~/Tcc_Luciano/scripts$ python exemplo_unitario_tests.py 
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK

\end{lstlisting}
\FloatBarrier

\subsection{Teste de Integração}
	Os testes de integração são utilizados para validar a integração de subsistemas e são aplicados após os testes de unidade. O uso dessa técnica na integração de grandes sistemas não é muito eficaz, pela dificuldade em efetuar correções caso sejam encontrados erros. 
	
	Para facilitar a localização de erros na integração é usado uma abordagem de integração incremental, que consiste em fazer a integração entre pequenos módulos, ou seja, para a integração entre três módulos, inicialmente integra-se dois módulos. Posteriormente, são executados os testes, sendo que, caso esses sejam executados com sucesso o terceiro módulo é integrado. Após a integração do terceiro módulo executa-se novamente as rotinas de testes de integração \cite{Pressman2006}.

\subsection{Teste de Regressão}

	Esta estratégia auxilia na localização de erros quando agregam-se mudanças ou correções de erros ao software, pois essas ações podem ocasionar a introdução de erros em funcionalidades já validadas. Se forem localizados erros nesta tarefa, é dito que sistema regrediu e inicia-se a fase de correção dos erros antes da liberação do sistema \cite{Delamaro2007}.
	
	A aplicação dessa estratégia consiste em reexecutar os casos de testes ao final do desenvolvimento das alterações para garantir que as novas mudanças não ocasionaram erros ao software. Essa estratégia pode ser executada de forma manual ou utilizando-se ferramentas automatizadas que facilitam o processo de teste \cite{Sommerville2003}.
	
\subsection{Teste de Sistema}
	Este teste constitui o processo mais difícil, uma vez que é aplicado a todo o software. Segundo \cite{Myers2004}, os casos de testes para esta técnica devem ser baseados nos objetivos do software, juntamente com a documentação do usuário. Essa abordagem de união entre os objetivos do software mais a documentação do usuário, aproxima-se de uma execução real do sistema.

	O teste de sistema apresenta como principal objetivo demonstrar que o sistema implementa todos os requisitos iniciais. Este teste é executado simulando um usuário final, sendo que o ambiente de teste é o mesmo que o usuário utilizaria em seu ambiente de trabalho, aumentando assim a probabilidade dos erros serem descobertos.


\subsection{Teste de Desempenho}
	Os testes de desempenho são testes que avaliam a performance do sistema, podendo ser executados ao longo de todo o desenvolvimento do software. Este tipo de teste pode ser empregado nos módulos do software à medida que eles vão sendo integrados e também ao final do desenvolvimento, como um todo. Nesse tipo de teste, podem ser testados por exemplo, o número de requisições que determinado módulo suporta, o uso de recursos de hardware, o tempo de uma consulta em um banco de dados, entre outros \cite{Pressman2006}.

\section{Considerações Finais}
	Este capítulo apresentou uma visão geral sobre testes de software. Foram descritas as principais técnicas de testes de software, apresentando como objeto de estudo as técnicas estruturais, funcionais e automatizados. Também foram apresentadas as principais estratégias de teste, tais como, teste de unidade, de integração, de regressão, de sistema e por fim teste de desempenho. 
	
	Na técnica de teste estrutural (caixa branca) é necessário que o desenvolvedor tenha conhecimento da estrutura interna do código, por outro lado, a técnica funcional (caixa preta) não aborda o funcionamento interno do código, verificando apenas se o sistema desenvolvido implementa as funcionalidades que foram definidas nos requisitos.
	
	Como foi definido anteriormente, o teste de unidade tem como principal objetivo localizar erros em pequenos trechos de códigos. O teste de integração visa validar a integração entre subsistemas. O teste de regressão é utilizado quando efetua-se alterações ou correções de erros ao software. O teste de sistema é aplicado na finalização do software, tendo por objetivo validar se as funcionalidades descritas nos requisitos iniciais foram de fato implementadas. Por fim, no teste desempenho a finalidade é avaliar a performance global de todo o software.
	
	Levando em consideração que o problema deste trabalho é definido em termos da validação das especificações a partir dos requisitos iniciais e baseado nos estudos das características dos testes, a técnica de teste funcional aliada à estratégia de teste unitário, são os mais adequadas para a solução do problema.